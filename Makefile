NAME=radioitaliastreaming
XSPF=$(NAME).xspf
XSL=$(NAME).xsl
CSS=$(NAME).css
SVG=$(NAME).svg

all: generate

validate:
	xmlstarlet validate -e -r http://www.xspf.org/validation/xspf-1_0.7.rng $(XSPF)

generate: validate
	xsltproc $(XSL) $(XSPF) > index.html

upload: generate
	HOME=. wput $(XSPF) $(SVG) $(CSS) index.html ftp://ftp.radioitaliastreaming.it/

.PHONY: upload
