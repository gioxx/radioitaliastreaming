<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xspf="http://xspf.org/ns/0/"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="xspf:track">
    <xsl:variable name="location">
      <xsl:value-of select="xspf:location"/>
    </xsl:variable>
    <xsl:variable name="info">
      <xsl:value-of select="xspf:info"/>
    </xsl:variable>

    <div class="track">
        <h4><xsl:value-of select="xspf:creator"/></h4>
        <p><a href="{$info}"><xsl:value-of select="xspf:title"/></a></p>
        <p>Streaming: <a href="{$location}"><xsl:value-of select="xspf:location"/></a></p>
    </div>
  </xsl:template>
</xsl:stylesheet>
