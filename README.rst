Radio Italia Streaming
======================
(The project is for Italian speakers, it does not make sense to write
in English here)

Il progetto è dedicato agli utenti che vogliono ascoltare radio in lingua
italiana da un riproduttore multimediale.

Radio Italia Streaming non è altro che una lista XSPF pubblicamente disponibile
`qui`_ e sul sito `Radio Italia Streaming`_.

Gli utenti possono scaricare questa lista ed aprirla con un qualsiasi
riproduttore multimediale che supporti il formato XSPF o che abbia un'apposita
estensione per Radio Italia Streaming.


Estensioni Disponibili
----------------------
Al momento sono disponibili le seguenti estensioni:

- `Amarok`_:  `Amarok Script`_
- `Kodi`_: `Kodi Addon`_

Per maggiori informazioni circa la loro installazione sui relativi riproduttori
multimediali, potete consultare il sito `Radio Italia Streaming`_ oppure
i rispettivi progetti.

.. _`qui`: https://gitlab.com/radioitaliastreaming/radioitaliastreaming
.. _`Radio Italia Streaming` : http://www.radioitaliastreaming.it/
.. _`Amarok` : https://amarok.kde.org/
.. _`Kodi`: https://kodi.tv/
.. _`Kodi Addon`: https://gitlab.com/radioitaliastreaming/ris-kodi-plugin
.. _`Amarok Script`: https://gitlab.com/radioitaliastreaming/ris-amarok-script
