<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xspf="http://xspf.org/ns/0/"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="radioitaliastreaming-track.xsl"/>

  <xsl:template match="/">
    <html lang="it">
      <head>
            <meta charset="UTF-8" />
            <meta name="author" content="Federico Vaga" />
            <meta name="description" content="Playlist streaming: Italian radios" />
            <meta name="keywords" content="playlist, radio, webradio, streaming, italia, italiano, svizzera" />
            <meta name="ocs-site-verification" content="eff0e48c134646c959447c528f0c6f15" />
            <base href="http://www.radioitaliastreaming.it/" target="_blank" />
            <link rel="stylesheet" type="text/css" href="radioitaliastreaming.css" />
            <title><xsl:value-of select="xspf:playlist/title"/></title>
      </head>
      <body>
        <xsl:apply-templates/>
      </body>
    </html>
  </xsl:template>


  <xsl:template match="xspf:playlist">
    <!-- HEADER -->
    <header>
      <h1><xsl:value-of select="xspf:title"/></h1>
      <p id="note"><xsl:value-of select="xspf:annotation"/></p>
      <img src="radioitaliastreaming.svg" />
      <p id="autore">
        Autore: <a href="http://www.federicovaga.it/">http://www.federicovaga.it/</a>
      </p>
      <p id="info">
        Radio Italia Streaming è una collezione di radio in lingua italiana.
        Per renderela fruibile in modo semplice ed ad un pubblico più vasto,
        ho deciso di utilizzare il formato <a href="http://www.xspf.org/">XSPF</a>.
      </p>
    </header>

    <!-- CONTENT -->
    <h1>Utilizzo</h1>
    <p>
      Per utilizzare la lista Radio Italia Streaming non dovete far altro che
      scaricare il file
      <a href="radioitaliastreaming.xspf">radioitaliastreaming.xspf</a>
      ed aprirlo con un riproduttore multimediale che supporti il formato
      <a href="http://www.xspf.org/">XSPF</a>.
    </p>
    <p>
      Per alcuni riproduttori multimediali si possono installare delle
      estensioni dedicate che permettono di avere Radio Italia Streaming sempre
      disponibile ed aggiornata. Al momento esistono estensioni per i seguenti
      riproduttori multimediali: <a href="https://amarok.kde.org/">Amarok</a>,
      <a href="https://kodi.tv/">Kodi</a>. L'installazione di un'estensione
      dipende dal riproduttore multimediale; per questo motivo, per la procedura
      d'installazione, vi rimando alle pagine dei rispettivi progetti.
      <ul>
        <li>
          <a href="https://gitlab.com/radioitaliastreaming/ris-amarok-script">
            Estensione per Amarok
          </a>
        </li>
        <li>
          <a href="https://gitlab.com/radioitaliastreaming/ris-kodi-plugin">
            Estensione per Kodi
          </a>
        </li>
      </ul>
    </p>

    <h1>Contribuire</h1>
    <p>
      Siete liberi di contribuire a questo modesto progetto. Potete trovare
      tutti i file sorgenti sulla piattaforma GitLab al seguente indirizzo:
      <a href="https://gitlab.com/radioitaliastreaming">https://gitlab.com/radioitaliastreaming</a>.
      Il progetto Radio Italia Streaming è suddiviso nei sotto-progetti
      elencati di seguito.
      <ul>
        <li>
          <a href="https://gitlab.com/radioitaliastreaming/radioitaliastreaming">
            Lista XSPF e sito internet
          </a>
        </li>
        <li>
          <a href="https://gitlab.com/radioitaliastreaming/ris-amarok-script">
            Estensione per Amarok
          </a>
        </li>
        <li>
          <a href="https://gitlab.com/radioitaliastreaming/ris-kodi-plugin">
            Estensione per Kodi
          </a>
        </li>
      </ul>
    </p>

    <xsl:apply-templates select="xspf:trackList"/>

    <!-- FOOTER -->
    <footer>
      <a href="http://jigsaw.w3.org/css-validator/check/referer">
        <img style="border:0;width:88px;height:31px"
             src="http://jigsaw.w3.org/css-validator/images/vcss"
             alt="Valid CSS!" />
      </a>
      <a href="http://validator.xspf.org/referrer/">
        <img style="border:0;width:88px;height:31px"
             src="http://svn.xiph.org/websites/xspf.org/images/banners/valid-xspf.png"
             alt="Valid XSPF Playlist" />
      </a>
    </footer>
  </xsl:template>


  <xsl:template match="xspf:trackList">
    <div class="trackList">
      <h1>Elenco delle radio</h1>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

</xsl:stylesheet>
